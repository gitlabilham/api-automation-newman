# API Automation Newman


# Content


- [Introduction](https://gitlab.com/gitlabilham/api-automation-newman#introduction)  

- [Collection of API](https://gitlab.com/gitlabilham/performance-testing-jmeterg#collection-of-api)
- [CLI Report](https://gitlab.com/gitlabilham/performance-testing-jmeterg#cli-report)   
- [HTML Report](https://gitlab.com/gitlabilham/performance-testing-jmeter#html-report) 



# Introduction

API Automation test using Newman on  [Secondhand Website](https://secondhand.binaracademy.org/)  



 
# Collection of API

- **List of API**

      - POST https://secondhand.binaracademy.org/users.json
      - POST https://secondhand.binaracademy.org/users/sign_in.json
      - GET https://secondhand.binaracademy.org/categories.json
      - GET https://secondhand.binaracademy.org/categories/{id}.json
      - POST https://secondhand.binaracademy.org/offers.json
      - GET https://secondhand.binaracademy.org/users/{user_id}/offers.json
      - PUT https://secondhand.binaracademy.org/offers/{id}.json
      - POST https://secondhand.binaracademy.org/products.json
      - GET https://secondhand.binaracademy.org/products.json
      - GET https://secondhand.binaracademy.org/products/{id}.json
      - PUT https://secondhand.binaracademy.org/products/{id}.json
      - DELETE https://secondhand.binaracademy.org/products/{id}.json
      - PUT https://secondhand.binaracademy.org/profiles.json
      - GET https://secondhand.binaracademy.org/profiles.json

    


# CLI Report



![1](https://gitlab.com/gitlabilham/api-automation-newman/-/raw/main/Assets/Screenshot%20CLI%20Report.png?ref_type=heads)    

# HTML Report

-  **Dashboard**

![2](https://gitlab.com/gitlabilham/api-automation-newman/-/raw/main/Assets/Screenshot%20Dashboard%20Newman%20Website%20Secondhand.png?ref_type=heads)   

- **Total Request**


![2](https://gitlab.com/gitlabilham/api-automation-newman/-/raw/main/Assets/Screenshot%20Newman%20Total%20Request.png?ref_type=heads) 